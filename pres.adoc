= IaC инструменты: Hashicorp Terraform
:customcss: ./css/custom.css
:revealjs_theme: league
:revealjsdir: reveal.js
:revealjs_width: 1408
:revealjs_height: 792
:source-highlighter: highlight.js
:highlightjs-languages: terraform
:icons: font

include::slides/00-presentation.adoc[]

include::slides/01-iac.adoc[]

include::slides/02-dev-iac.adoc[]

include::slides/03-tf-overview.adoc[]

include::slides/04-tf-syntax.adoc[]

include::slides/05-tf-cli.adoc[]

include::slides/06-final.adoc[]

