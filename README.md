Для работы презентации в папку с презентацией нужно положить reveal.js

```bash
git clone -b 4.1.2 --depth 1 https://github.com/hakimel/reveal.js.git
```

Для сборки презентации использовался конвертер [Asciidoctor reveal.js](https://docs.asciidoctor.org/reveal.js-converter/latest/)

- [Установка из NPM](https://docs.asciidoctor.org/reveal.js-converter/latest/setup/node-js-setup/)
- [Установка standalone executable](https://docs.asciidoctor.org/reveal.js-converter/latest/setup/standalone-executable/)

Сборка:

```bash
asciidoctor-revealjs pres.adoc
```